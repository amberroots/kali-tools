---
Title: sfuzz
Homepage: http://aconole.brad-x.com/programs/sfuzz.html
Repository: https://gitlab.com/kalilinux/packages/sfuzz
Architectures: any
Version: 0.7.0-1kali4
Metapackages: kali-linux-everything kali-linux-large kali-tools-fuzzing kali-tools-vulnerability 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### sfuzz
 
  In the same vein as the Generic Protocol Framework, sfuzz is
  a really simple to use black box testing suite called Simple
  Fuzzer (what else would you expect?). The goal is to provide
  a simple to use, but fairly powerful and flexible black box
  testing utility.
 
 **Installed size:** `191 KB`  
 **How to install:** `sudo apt install sfuzz`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 {{< /spoiler >}}
 
 ##### sfo
 
 
 ```
 root@kali:~# sfo -h
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [604057].
 [604057] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [604284].
 [604284] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [604526].
 [604526] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [604767].
 [604767] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [605009].
 [605009] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [605252].
 [605252] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [605490].
 [605490] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [605743].
 [605743] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [605998].
 [605998] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [606247].
 [606247] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [606492].
 [606492] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [606744].
 [606744] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [606995].
 [606995] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [607240].
 [607240] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [607467].
 [607467] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [607688].
 [607688] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [607913].
 [607913] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [608135].
 [608135] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [608357].
 [608357] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [608582].
 [608582] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [608799].
 [608799] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [609012].
 [609012] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [609221].
 [609221] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [609452].
 [609452] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [609692].
 [609692] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [609929].
 [609929] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [610170].
 [610170] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [610417].
 [610417] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [61065
 ```
 
 - - -
 
 ##### sfuzz
 
 
 ```
 root@kali:~# sfuzz -h
 		Simple Fuzzer
 By:	 Aaron Conole
 version: 0.7.0
 url:	 http://aconole.brad-x.com/programs/sfuzz.html
 EMAIL:	 apconole@yahoo.com
 Build-prefix: /usr
 	-h	 This message.
 	-V	 Version information.
 
 networking / output:
 	-v	 Verbose output
 	-q	 Silent output mode (generally for CLI fuzzing)
 	-X	 prints the output in hex
 
 	-b	 Begin fuzzing at the test specified.
 	-e	 End testing on failure.
 	-t	 Wait time for reading the socket
 	-S	 Remote host
 	-p	 Port
 	-T|-U|-O TCP|UDP|Output mode
 	-R	 Refrain from closing connections (ie: "leak" them)
 
 	-f	 Config File
 	-L	 Log file
 	-n	 Create a new logfile after each fuzz
 	-r	 Trim the tailing newline
 	-D	 Define a symbol and value (X=y).
 	-l	 Only perform literal fuzzing
 	-s	 Only perform sequence fuzzing
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}

## sfuzz Usage Example

Fuzz the target server (`-S 192.168.1.1`) on port 10443 (`-p 10443`) with TCP output mode (`-T`), using the basic HTTP config (`-f /usr/share/sfuzz/sfuzz-sample/basic.http`):

```console
root@kali:~# sfuzz -S 192.168.1.1 -p 10443 -T -f /usr/share/sfuzz/sfuzz-sample/basic.http
[12:53:47] dumping options:
    filename: </usr/share/sfuzz/sfuzz-sample/basic.http>
    state:    <8>
    lineno:   <56>
    literals:  [74]
    sequences: [34]
    symbols: [0]
    req_del:  <200>
    mseq_len: <10024>
    plugin: <none>
    s_syms: <0>
    literal[1] = [AREALLYBADSTRING]
```
